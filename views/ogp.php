<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140595741-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-140595741-1');
</script>
<meta charset="utf-8" />
<base href="/" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="<?=h($tag);?>" />
<meta name="description" content="<?=h($description);?>" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@bok3_0" />
<meta property="og:title" content="<?=h($title);?>"/>
<meta property="og:type" content="article">
<meta property="og:url" content="<?=h($url);?>">
<meta property="og:image" content="<?=h($thumbnail);?>">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="628">
<meta property="og:site_name" content="<?=h($site_name);?> - 木瓜丸屋">
<meta property="og:description" content="<?=h($description);?>">
<title><?=h($title);?> - 木瓜丸屋</title>
<link rel="stylesheet" href="style.css">
