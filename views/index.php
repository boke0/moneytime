<!DOCTYPE html>
<html>
<head>
    <?php //$this->draw("header");?>
    <link rel="stylesheet" href="/style.css">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="/main.js"></script>
</head>
<body>
    <div id="stage"></div>
    <template id="top">
        <mn-header>
            <mn-menu></mn-menu>
            <mn-header-title>
                探す
            </mn-header-title>
        </mn-header>
        <main></main>
        <mn-bottom-menu></mn-bottom-menu>
    </template>
    <template id="day">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-title id="date">
            </mn-header-title>
        </mn-header>
        <main></main>
        <script>
            
        </script>
    </template>
    <template id="job">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-title id="date">
            </mn-header-title>
        </mn-header>
        <main></main>
        <mn-menu-bar></mn-menu-bar>
    </template>
    <template id="comment">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-title>
                コメント
            </mn-header-title>
        </mn-header>
    </template>
    <template id="aboutme">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-title>
                職歴・自己PR
            </mn-header-title>
        </mn-header>
    </template>
    <template id="notice">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-tab>
                <mn-header-tab-title for="notification">
                    お知らせ
                </mn-header-tab-title>
                <mn-header-tab-title for="news">
                    ニュース
                </mn-header-tab-title>
            </mn-header-tab>
        </mn-header>
        <main>
            <div id="notification"></div>
            <div id="news"></div>
        </main>
    </template>
    <template id="accept">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-title id="date">
            </mn-header-title>
        </mn-header>
    </template>
    <template id="message">
        <mn-header>
            <mn-back></mn-back>
            <mn-header-title id="date">
            </mn-header-title>
        </mn-header>
    </template>
</body>
</html>
