<?php
namespace admin;
class sessionCtrl extends \Ctrl{
    private $admin;
    private $me;
    public function __construct(){
        parent::__construct();
        $this->admin=$this->instance("admin");
        $this->me=$this->admin->getSession($this->cookie->get("admin_jwt"));
    }
    public function indexAct(){
        if(!$this->me){
            header("Location: ./login");
            exit;
        }
        $this->view->draw("admin/index");
    }
    public function setupAct(){
        if($this->me){
            header("Location: ./");
            exit;
        }
        if(!$this->admin->checkSetupNeed()){
            header("Location: ./login");
            exit;
        }
        if($this->server->get("REQUEST_METHOD")=="POST"){
            $this->view->csrfCheck();
            $post=$this->post->get();
            $error=$this->admin->register($post["id"],$post["name"],$post["email"],$post["password"]);
            if(!$error){
                header("Location: ./");
                exit;
            }
        }else{
            $this->view->csrfSet();
        }
        $this->view->draw("admin/setup",[
            "post"=>$post,
            "error"=>$error
        ]);
    }
    public function loginAct(){
        if($this->me){
            header("Location: ./");
            exit;
        }
        if($this->server->get("REQUEST_METHOD")=="POST"){
            $this->view->csrfCheck();
            $post=$this->post->get();
            $token=$this->admin->login($post["email"],$post["password"]);
            $this->cookie->set("admin_jwt",$token,14*24*3600);
            header("Location: ./");
            exit;
        }else{
            $this->view->csrfSet();
        }
        $this->view->draw("admin/login",[
            "post"=>$post
        ]);
    }
    public function logoutAct(){
        $this->cookie->delete("admin_jwt");
        header("Location: ./");
        exit;
    }
}
