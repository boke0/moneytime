<?php

class friendshipCtrl extends Ctrl{
    public function __construct(){
        parent::__construct();
        $this->user=$this->instance("user");
        $this->friendship=$this->instance("friendship");
        $this->me=$this->user->session($this->cookie->get("jwt_token"));
    }
    public function createAct(){
        $id=$this->request->get("id");
        $this->friendship->create($this->me["id"],$id);
    }
    public function deleteAct(){
        $id=$this->request->get("id");
        $this->friendship->delete($this->me["id"],$id);
    }
}
