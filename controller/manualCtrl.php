<?php

class manualCtrl extends Ctrl{
    public function overviewAct(){
        $this->view->draw("manual/index");
    }
    public function categoryAct($category){
        $this->view->draw("manual/{$category}/index");
    }
    public function itemAct($category,$id){
        $this->view->draw("manual/{$category}/{$id}");
    }
}
