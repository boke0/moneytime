<?php

require_once("../config.php");
require_once("../core/functions.php");
require_once("../core/View.php");
require_once("../core/Ctrl.php");
require_once("../core/Mdl.php");
require_once("../core/Variables.php");
require_once("../core/QueryString.php");
require_once("../core/Request.php");
require_once("../core/Files.php");
require_once("../core/Server.php");
require_once("../core/Cookie.php");
require_once("../core/Router.php");
//require_once("../core/Session.php");

require("../routes.php");
