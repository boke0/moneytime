import anker from "./part/anker.js";
import backBtn from "./part/backBtn.js";
import bottomTabContainer from "./part/bottomTabContainer.js";
import bottomTabTitle from "./part/bottomTabTitle.js";
import menuBar from "./part/menuBar.js";
import tabContainer from "./part/tabContainer.js";
import tabTitle from "./part/tabTitle.js";
import tabPage from "./part/tabPage.js";
import header from "./part/header.js";
import headerTabContainer from "./part/headerTabContainer.js";
import headerTabTitle from "./part/headerTabTitle.js";
import headerTitle from "./part/headerTitle.js";
import menuBtn from "./part/menuBtn.js";
import applyBar from "./part/applyBar.js";

customElements.define("mn-a",anker);
customElements.define("mn-back",backBtn);
customElements.define("mn-bottom-tab",bottomTabContainer);
customElements.define("mn-bottom-tab-title",bottomTabTitle);
customElements.define("mn-bottom-menu",menuBar);
customElements.define("mn-tab",tabContainer);
customElements.define("mn-tab-title",tabTitle);
customElements.define("mn-tab-page",tabPage);
customElements.define("mn-header",header);
customElements.define("mn-header-tab",headerTabContainer);
customElements.define("mn-header-tab-title",headerTabTitle);
customElements.define("mn-header-title",headerTitle);
customElements.define("mn-menu",menuBtn);
customElements.define("mn-apply-bar",applyBar);

const routes={
    "/":"#top",
    "/mypage":"#aboutme",
    "/notification":"#notice",
    "/apply/:id":"#accept",
//    "/messages":document.querySelector("#message"),
    "/:date":"#day",
    "/:date/:job":"#job",
    "/:date/:job/comments":"#comment"
}
window.addEventListener("popstate",()=>{
    window.dispatchEvent(new Event("pushstate"));
});
window.addEventListener("pushstate",()=>{
    var url=new URL(location.href);
    const path=url.pathname.split("/");
    var route;
    Object.keys(routes).some((p,i,a)=>{
        const path_=p.split("/");
        const path__=path.concat();
        let params={};
        while(path__.length*path_.length>0){
            let s=path_.shift();
            let t=path__.shift();
            if(s!=t&&s.substr(0,1)!=":"){
                return false;
            }else if(s.substr(0,1)==":"){
                params[s.slice(1)]=t;
            }
        }
        if(routes[p]&&path__.length==0){
            route=routes[p];
            window.urlParams=params;
            return true;
        }
    });
    document.querySelector("#stage").innerHTML="";
    document.querySelector("#stage").appendChild(document.importNode(document.querySelector(route).content,true));
});
window.onload=()=>{
    window.dispatchEvent(new Event("pushstate"));
}
