export default class headerTabContainer extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:flex;
                flex-direction:row;
                justify-content:space-around;
            }
        `;
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        this.shadow.appendChild(slot);
    }
    connectedCallback(){
        this.querySelector("mn-header-tab-title").click();
    }
}
