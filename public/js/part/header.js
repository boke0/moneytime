export default class header extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                height:48px;
                padding:0px 8px;
                display:grid;
                box-sizing:border-box;
                grid-template-columns:32px 1fr;
                background:;
                color:#fff;
                align-items:center;
                position:sticky;
                top:0;
                left:0;
                width:100%;
                background:#ec6800;
                line-height40px;
                box-shadow:0 0 10px -5px #000;
            }
        `;
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        this.shadow.appendChild(slot);
    }
}
