export default class headerTabTitle extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:block;
                color:inherit;
                height:48px;
                line-height:48px;
                text-align:center;
                flex-grow:1;
            }
            div{
                border-bottom:2px solid #fff0;
                width:100%;
                height:100%;
                text-align:center;
                transition:border-color .3s;
                box-sizing:border-box;
            }
            div.open{
                border-bottom:2px solid #ffff;
            }
        `;
        const container=document.createElement("div");
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        container.appendChild(slot);
        this.container_=container;
        this.shadow.appendChild(container);
        this.onclick=()=>{
            this.parentNode.querySelectorAll("mn-header-tab-title").forEach(e=>{e.close();});
            this.show();
            document.querySelectorAll("main>div").forEach(e=>{e.style.display="none";});
            document.querySelector("main>#"+this.for).style.display="block";
        }
    }
    set for(value){
        return this.setAttribute("for",value);
    }
    get for(){
        return this.getAttribute("for");
    }
    show(){
        this.container_.classList.add("open");
    }
    close(){
        this.container_.classList.remove("open");
    }
}
