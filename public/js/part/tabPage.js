export default class tabPage extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:none;
                width:100%;
                height:auto;
                background:inherit;
                color:inherit;
            }
            :host(.open){
                display:block;
            }
        `;
        const slot=document.createElement(slot);
        this.shadow.appendChild(style);
        this.shadow.appendChild(slot);
    }
    show(){
        this.classList.add("open");
    }
    close(){
        this.classList.remove("open");
    }
}
