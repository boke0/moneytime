export default class tabContainer extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
        `;
        const slot_title=document.createElement("slot");
        slot_title.name="title";
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        this.shadow.appendChild(slot_title);
        this.shadow.appendChild(slot);
    }
    connectedCallback(){
        this.shadow.querySelector("slot[name='title']").click();
    }
}
