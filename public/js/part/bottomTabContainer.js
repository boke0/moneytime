export default class bottomTabContainer extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                height:56px;
                display:flex;
                flex-direction:row;
                justify-content:space-around;
                background:#fff;
                box-shadow:0 0 10px -5px #000;
                color:#fff;
                width:100%;
            }
        `;
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        this.shadow.appendChild(slot);
    }
    connectedCallback(){
        window.addEventListener("pushstate",()=>{
            this.querySelector("mn-bottom-tab-title").close();
            this.querySelector("[href='"+location.pathname+"']").show();
        });
    }
}
