export default class anker extends HTMLElement{
    constructor(){
        super();
        this.onclick=()=>{
            history.pushState(null,null,this.href);
            window.dispatchEvent(new Event("pushstate"));
        }
    }
    set href(value){
        this.getAttribute("href",value);
    }
    get href(){
        return this.getAttribute("href");
    }
}
