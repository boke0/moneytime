export default class applyBar extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:flex;
                justify-content:flex-end;
                height:56px;
                width:100%;
                padding:8px;
                box-sizing:border-box;
                background:#fff;
                box-shadow:0 0 10px -5px #000;
            }
            button{
                height:40px;
                line-height:40px;
                color:#fff;
                padding:0 16px;
                font-weight:bold;
                font-size:0.8em;
                background:#ec6800;
            }
        `;
        const button=document.createElement("button");
        button.innerHTML="申請";
        this.shadow.appendChild(style);
        this.shadow.appendChild(button);
    }
}
