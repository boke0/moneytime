export default class bottomTabTitle extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:block;
                width:56px;
                height:56px;
            }
            #container{
                display:flex;
                justify-content:center;
                align-items:center;
                flex-direction: column;
                height:56px;
                width:56px;
            }
            svg{
                fill:#777;
                height:24px;
                width:24px;
                margin-bottom:4px;
            }
            #container.open svg{
                fill:#ec6800;
            }
            #text{
                color:#000;
                font-size:0.5em;
                font-weight:bold;
            }
        `;
        const container=document.createElement("div");
        container.id="container";
        this.container_=container;
        const icon=document.createElementNS("http://www.w3.org/2000/svg","svg");
        const icon_use=document.createElementNS("http://www.w3.org/2000/svg","use");
        const text=document.createElement("div");
        text.id="text";
        this.use_=icon_use;
        this.title_=text;
        icon.appendChild(icon_use);
        this.shadow.appendChild(style);
        container.appendChild(icon);
        container.appendChild(text);
        this.shadow.appendChild(container);
        this.onclick=()=>{
            history.pushState(null,null,this.href);
            window.dispatchEvent(new Event("pushstate"));
        }
    }
    show(){
        this.container_.classList.add("open");
    }
    close(){
        this.container_.classList.remove("open");
    }
    set iconref(url){
        this.use_.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href",url);
    }
    set title(value){
        this.title_.innerText=value;
    }
    set href(url){
        this.setAttribute("href",url);
    }
    get href(){
        return this.getAttribute("href");
    }
}
