export default class backBtn extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                height:32px;
                width:32px;
                position:relative;
            }
            :host::after{
                content:"";
                display:block;
                position:absolute;
                width:8px;
                height:8px;
                border-top:2px solid #fff;
                border-left:2px solid #fff;
                transform:rotate(-45deg);
                transform-origin:center;
                top:11px;
                left:11px;
            }
        `;
        this.shadow.appendChild(style);
        this.onclick=()=>{
            history.back();
        }
    }
}
