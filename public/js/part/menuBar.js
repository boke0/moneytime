export default class menuBar extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                position:fixed;
                bottom:0;
                width:100%;
                left:0;
            }
        `;
        const tab=document.createElement("mn-bottom-tab");
        const tab_search=document.createElement("mn-bottom-tab-title");
        tab_search.title="検索";
        tab_search.href="/";
        tab_search.iconref="/img/spritesheet.svg#search";
        const tab_incom=document.createElement("mn-bottom-tab-title");
        tab_incom.title="募集";
        tab_incom.href="/income";
        tab_incom.iconref="/img/spritesheet.svg#income";
        const tab_notice=document.createElement("mn-bottom-tab-title");
        tab_notice.title="通知";
        tab_notice.href="/notice";
        tab_notice.iconref="/img/spritesheet.svg#notice";
        const tab_message=document.createElement("mn-bottom-tab-title");
        tab_message.title="メッセージ";
        tab_message.href="/message";
        tab_message.iconref="/img/spritesheet.svg#message";
        const tab_setting=document.createElement("mn-bottom-tab-title");
        tab_setting.title="設定";
        tab_setting.href="/setting";
        tab_setting.iconref="/img/spritesheet.svg#setting";
        tab.appendChild(tab_search);
        tab.appendChild(tab_incom);
        tab.appendChild(tab_notice);
        tab.appendChild(tab_message);
        tab.appendChild(tab_setting);
        this.shadow.appendChild(style);
        this.shadow.appendChild(tab);
    }
}
