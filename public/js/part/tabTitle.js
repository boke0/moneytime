export default class tabTitle extends HTMLElement{
    constructor(){
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:inline-block;
                color:inherit;
                height:32px;
                line-height:32px;
                border-radius:6px 6px 0 0;
            }
            div{
                background:#0001;
                width:100%;
                height:100%;
                padding:0 16px;
            }
            .open div{
                background:#0000;
            }
        `;
        const container=document.createElement("div");
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        container.appendChild(slot);
        this.container_=container;
        this.slot="title";
        this.shadow.appendChild(container);
        this.onclick=()=>{
            this.parentNode.querySelectorAll("mn-tab-title").forEach(e=>{e.close();});
            this.show();
            this.parentNode.querySelectorAll("mn-tab-page").forEach(e=>{e.close();});
            this.parentNode.querySelector("#"+this.for).show();
        }
    }
    set for(value){
        return this.setAttribute("for",value);
    }
    get for(){
        return this.getAttribute("for");
    }
    show(){
        this.container_.classList.add("open");
    }
    close(){
        this.container_.classList.remove("open");
    }
}
