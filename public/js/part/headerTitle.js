export default class headerTabTitle extends HTMLElement{
    constructor(){
        super();
        this.shadow=this.attachShadow({mode:"closed"});
        const style=document.createElement("style");
        style.innerHTML=`
            :host{
                display:block;
                color:inherit;
                height:40px;
                line-height:40px;
                text-align:center;
                font-weight:bold;
            }
        `;
        const slot=document.createElement("slot");
        this.shadow.appendChild(style);
        this.shadow.appendChild(slot);
    }
}
