const gulp=require("gulp");
const sass=require("gulp-sass");
const plumber=require("gulp-plumber");
const autoprefixer=require("gulp-autoprefixer");
const webpack=require("webpack");
const webpackStream=require("webpack-stream");
const config=require("./webpack.config.js");

gulp.task("sass",()=>{
    return gulp.src("sass/style.scss")
        .pipe(plumber({
            errorHandler:function(e){
                console.log(e.messageFormatted);
                this.emit("end");
            }
        }))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest("./"));
});
gulp.task("js",()=>{
    return webpackStream(config,webpack).pipe(gulp.dest("./"));
})
gulp.task("default",()=>{
    gulp.watch("sass/*",gulp.task("sass"));
    gulp.watch("js/*",gulp.task("js"));
    gulp.watch("js/*/*",gulp.task("js"));
})
