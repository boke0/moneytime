<?php

$router=new Router();

$router->setRootPath("/");

$router->setRoute("admin/login","admin\session.login");
$router->setRoute("admin/setup","admin\session.setup");
$router->setRoute("admin/logout","admin\session.logout");
$router->setRoute("","page.index");
$router->setRoute("ep/session/get.json","user.session",["GET"]);
$router->setRoute("ep/user/list.json","user.list",["GET"]);
$router->setRoute("ep/user/get.json","user.get",["GET"]);
$router->setRoute("ep/user/update.json","user.update",["PUT"]);
$router->setRoute("ep/user/delete.json","user.delete",["DELETE"]);
$router->setRoute("ep/job/list.json","job.list",["GET"]);
$router->setRoute("ep/job/get.json","job.get",["GET"]);
$router->setRoute("ep/job/create.json","job.create",["POST"]);
$router->setRoute("ep/job/update.json","job.update",["PUT"]);
$router->setRoute("ep/job/delete.json","job.delete",["DELETE"]);
$router->setRoute("ep/notification/list.json","notification.list",["GET"]);
$router->setRoute("ep/notification/get.json","notification.get",["GET"]);

$router->dispatch();
