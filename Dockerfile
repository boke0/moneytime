FROM centos:latest
RUN yum -y update
RUN yum -y install epel-release
RUN yum -y install http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN yum -y install --enablerepo="remi-php73" httpd php php-pdo php-mysqlnd php-zip php-openssl openssl
CMD httpd -D FOREGROUND
