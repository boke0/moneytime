<?php

class friendshipMdl extends Mdl{
    public function __construct(){
        parent::__construct();
    }
    public function get($from,$to){
        return $this->query("select `from`,`to`,id,datetime from friendship where `from`=:from and `to`=:to limit 1",[":to"=>$to,":from"=>$from]);
    }
    public function create($from,$to){
        if($this->get($from,$to)!=FALSE){
            throw new Exception("Friendship already exists");
        }
        $this->query("insert into friendship (`from`,`to`,datetime) values (:from,:to,now())",[
            ":from"=>$from,
            ":to"=>$to
        ]);
        return $this->lastInsertId("id");
    }
    public function delete($from,$to){
        $this->query("delete from friendship where `from`=:from and `to`=:to limit 1",[
            ":from"=>$from,
            ":to"=>$to
        ]);
    }
}
