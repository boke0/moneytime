<?php

class adminMdl extends Mdl{
    public function __construct(){
        $this->connect(DSN,DBUSER,DBPASS);
    }
    public function login($em,$pw){
        $id=$this->dbh->query("select id from admin where email=:em and password=encrypt(:pw,:em)",[
            ":em"=>$em,
            ":pw"=>$pw
        ]);
        if($id){
            $header=array(
                "typ"=>"JWT",
                "alg"=>"HS256"
            );
            $payload=array(
                "id"=>$id["id"],
                "iat"=>time()
            );
            $token_=base64_encode(json_encode($header)).".".base64_encode(json_encode($payload));
            return $token_.".".hash_hmac("sha256",$token_,TOKEN_KEY);
        }else{
            return FALSE;
        }
    }
    public function idExists($id){
        return $this->query("select * from admin where id=:id",[
            ":id"=>$id
        ])?TRUE:FALSE;
    }
    public function emailExists($em){
        return $this->query("select * from admin where email=:em",[
            ":em"=>$em
        ])?TRUE:FALSE;
    }
    public function getSession($token){
        list($header,$payload,$token_)=explode(".",$token);
        if($token_==hash_hmac("sha256","$header.$payload",TOKEN_KEY)){
            $payload_=base64_decode($payload);
            $user=$this->dbh->query("select id,name from admin where id=:id",[
                ":id"=>$payload_["id"]
            ]);
            return $user;
        }else{
            return FALSE;
        }
    }
    public function register($id,$name,$em,$pw){
        $error=[];
        if($this->idExists($id)){
            $error["id"]="IDがすでに登録されています";
        }
        if($this->emailExists($em)){
            $error["email"]="メールアドレスがすでに登録されています";
        }
        if(empty($error)){
            $this->query("insert into admin (id,name,email,password) values (:id,:nm,:em,encrypt(:pw,:em))",[
                ":id"=>$id,
                ":nm"=>$name,
                ":em"=>$em,
                ":pw"=>$pw
            ]);
            return $this->lastInsertId("id");
        }else{
            return $error;
        }
    }
    public function checkSetupNeed(){
        return $this->query("select count(*) as c from admin")["c"]==0;
    }
}
