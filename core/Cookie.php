<?php

class Cookie extends Variables{
    protected function prepare(){
        foreach($_COOKIE as $k=>$v){
            $this->values[$k]=$v;
        }
    }
    public function set($k,$v,$e=0,$p="/"){
        setcookie($k,$v,time()+$e,$p);
        $this->values[$k]=$v;
    }
    public function delete($k){
        setcookie($k,NULL,0,"/");
    }
}
