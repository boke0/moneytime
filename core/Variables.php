<?php

abstract class Variables{
    protected $values;
    public function __construct(){
        $this->prepare();
    }
    protected function prepare(){}
    public function get($k=NULL){
        return $k?$this->values[$k]:$this->values;
    }
    public function has($k){
        return isset($this->values[$k]);
    }
}
