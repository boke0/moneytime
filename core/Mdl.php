<?php

abstract class Mdl{
    private $dbh;
    protected function connect($dsn,$dbname,$dbpass){
        $this->dbh=new PDO($dsn,$dbname,$dbpass,[PDO::ATTR_EMULATE_PREPARES=>FALSE]);
    }
    protected function query($sql,$a=[],$l=FALSE){
        $stmt=$this->dbh->prepare($sql);
        foreach($a as $k=>$v){
            switch(gettype($v)){
                case "integer":
                    $stmt->bindValue($k,$v,PDO::PARAM_INT);
                    break;
                case "string":
                    $stmt->bindValue($k,$v,PDO::PARAM_STR);
                    break;
                case "boolean":
                    $stmt->bindValue($k,$v,PDO::PARAM_BOOL);
                    break;
                case "NULL":
                    $stmt->bindValue($k,$v,PDO::PARAM_NULL);
                    break;
            }
        }
        $stmt->execute();
        return $l?$stmt->fetchAll(PDO::FETCH_ASSOC):$stmt->fetch(PDO::FETCH_ASSOC);
    }
    protected function lastInsertId($col){
        return $this->dbh->lastInsertId($col);
    }
}
