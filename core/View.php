<?php
require("../core/Parsedown.php");
class View{
    private $cookie;
    private $request;
    public function __construct(){
        $this->cookie=new Cookie();
        $this->request=new Request();
        $this->mdparse=new Parsedown();
    }
    public function csrfSet(){
        $token=sha1(uniqid().mt_rand());
        $this->cookie->set("csrf_token",$token,24*3600);
    }
    public function draw($p,$a=[]){
        foreach($a as $k=>$v){
            $$k=$v;
        }
        require("../views/{$p}.php");
    }
    public function csrfToken(){
        return $this->cookie->get("csrf_token");
    }
    public function csrfField(){
        $token=$this->cookie->get("csrf_token");
        return "<input type='hidden' name='csrf_token' value='{$token}'>";
    }
    public function csrfCheck(){
        $cookie=$this->cookie->get("csrf_token");
        $request=$this->request->get("csrf_token");
        if($cookie!=$request){
            header("HTTP/1.1 403 Forbidden");
            exit;
        }
    }
}
