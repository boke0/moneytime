<?php
class Request extends Variables{
    protected function prepare(){
        $server=new Server();
        switch($server->get("REQUEST_METHOD")){
            case "POST":
                if($server->get("CONTENT_TYPE")=="application/json"){
                    $this->values=json_decode(file_get_contents("php://input"));
                }else{
                    foreach($_POST as $k=>$v){
                        $this->values[$k]=$v;
                    }
                }
                break;
            default:
                if($server->get("CONTENT_TYPE")=="application/json"){
                    $this->values=json_decode(file_get_contents("php://input"));
                }else{
                    $lines=explode("&",file_get_contents("php://input"));
                    foreach($lines as $line){
                        $cell=explode("=",$line);
                        $this->values[$cell[0]]=$cell[1];
                    }
                }
                break;
        }
    }
}
