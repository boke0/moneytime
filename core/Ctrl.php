<?php
abstract class Ctrl{
    public function __construct(){
        $this->files=new Files();
        $this->querystring=new QueryString();
        $this->request=new Request();
        $this->server=new Server();
        $this->cookie=new Cookie();
        $this->view=new View();
        //$this->session=new Session();
    }
    protected function instance($m){
        require("../model/{$m}Mdl.php");
        $mdlname="{$m}Mdl";
        return new $mdlname();
    }
    protected function json_dump($arr){
        header("Content-Type: application/json");
        echo json_encode($arr);
        exit;
    }
}
