<?php

class Router{
    private $map;
    private $root;
    public function __construct(){
        $this->map=[
            "children"=>[],
            "routes"=>[]
        ];
    }
    public function setRootPath($path){
        $this->root=$path;
    }
    public function setRoute($path,$func,$method=["GET","POST"]){
        $direcs=explode("/",$path);
        $last=array_pop($direcs);
        $c=&$this->map;
        foreach((array)$direcs as $direc){
            if(isset($c["children"][$direc])){
                $c=&$c["children"][$direc];
            }else{
                $c["children"][$direc]=[
                    "routes"=>[],
                    "children"=>[]
                ];
                $c=&$c["children"][$direc];
            }
        }
        $c["routes"][$last]=[
            "method"=>$method,
            "call"=>$func
        ];
    }
    public function dispatch(){
        $server=new Server();
        $req=substr(explode("?",$server->get("REQUEST_URI"))[0],strlen($this->root));
        $method=$server->get("REQUEST_METHOD");
        $direcs=explode("/",$req);
        if($direcs[0]=="") array_shift($direcs);
        if(end($direcs)=="") array_pop($direcs);
        $last=array_pop($direcs);
        try{
            $c=$this->map;
            $params=[];
            ksort($c["children"]);
            foreach($direcs as $direc){
                if(isset($c["children"][$direc])){
                    $c=$c["children"][$direc];
                    ksort($c["children"]);
                }else{
                    $keys=array_keys($c["children"]);
                    for($i=0;$i<count($keys);$i++){
                        if(substr($keys[$i],0,1)==":"){
                            array_push($params,$direc);
                            $c=$c["children"][$keys[$i]];
                            ksort($c["children"]);
                        }
                        break;
                    }
                }
            }
            if(isset($c["routes"][$last])){
                $call=$last;
            }else{
                $keys=array_keys($c["routes"]);
                for($i=0;$i<count($keys);$i++){
                    if(substr($keys[$i],0,1)==":"){
                        array_push($params,$last);
                        $call=$keys[$i];
                    }
                    break;
                }
            }
            if(array_search($server->get("REQUEST_METHOD"),(array)$c["routes"][$call]["method"])!==FALSE){
                list($ctrl,$act)=explode(".",$c["routes"][$call]["call"]);
                $ctrlname="{$ctrl}Ctrl";
                $actname="{$act}Act";
                $filename=str_replace("\\","/",$ctrlname);
                require(DOCUMENT_ROOT."controller/$filename.php");
                $CTRL=new $ctrlname();
                call_user_func_array(array($CTRL,$actname),(array)$params);
            }
        }catch(Exception $e){
            header("HTTP/1.1 404 Not Found");
            echo file_get_contents("../404.html");
            exit;
        }
    }
}
