<?php
class Session extends Variables{
    protected function prepare(){
        session_start();
        foreach($_SESSION as $k=>$v){
            $this->values[$k]=$v;
        }
    }
    public function set($k,$v){
        $_SESSION[$k]=$v;
    }
    public function delete($k){
        unset($_SESSION[$k]);
    }
}
